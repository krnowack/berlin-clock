import java.util.Arrays;

/*
    Berlin Clock
    Create a representation of the Berlin Clock for a given time (hh::mm:ss).

    The Berlin Uhr (Clock) is a rather strange way to show the time.
    On the top of the clock there is a yellow lamp that blinks on/off every two seconds.
    The time is calculated by adding rectangular lamps.

    The top two rows of lamps are red. These indicate the hours of a day. In the top row there are 4 red lamps.
    Every lamp represents 5 hours. In the lower row of red lamps every lamp represents 1 hour.
    So if two lamps of the first row and three of the second row are switched on that indicates 5+5+3=13h or 1 pm.

    The two rows of lamps at the bottom count the minutes. The first of these rows has 11 lamps, the second 4.
    In the first row every lamp represents 5 minutes.
    In this first row the 3rd, 6th and 9th lamp are red and indicate the first quarter, half and last quarter of an hour.
    The other lamps are yellow. In the last row with 4 lamps every lamp represents 1 minute.

    The lamps are switched on from left to right.

    Y = Yellow
    R = Red
    O = Off
*/

public class BerlinClock {
    public String getClock(String time) {
        StringBuilder sb = new StringBuilder();
        sb.append(getSecondsLamp(time));
        sb.append(getFiveHoursRow(time));
        sb.append(getSingleHourRow(time));
        sb.append(getFiveMinutesRow(time));
        sb.append(getSingleMinutesRow(time));
        return sb.toString();
    }

    String getSingleMinutesRow(String time) {
        final int minutes = getMinutes(time);
        final int minutesPerOneLamp = 5;
        final int countOfSingleMinutes = minutes % minutesPerOneLamp;
        switch(countOfSingleMinutes)
        {
            case 0:
                return "OOOO";
            case 1: 
                return "YOOO";
            case 2:
                return "YYOO";
            case 3:
                return "YYYO";
            case 4:
                return "YYYY";
            default:
                return "OOOO"; // maybe throw exception
        }  
    }

    String getFiveMinutesRow(String time) {
        final int minutes = getMinutes(time);
        final int minutesPerOneLamp = 5;
        char lamps [] = new char[11];
        Arrays.fill(lamps, 'O');

        int idx = 0;
        for(int m = 1; m <= minutes; ++m){
            if(m == 15 || m == 30 || m == 45){
                lamps[idx++] = 'R';
            }else if(m % minutesPerOneLamp == 0){
                lamps[idx++] = 'Y';
            }
        }
        return new String(lamps);
    }

    String getSingleHourRow(String time) {
        final int hours = getHours(time);
        final int hoursPerOneLamp = 5;
        final int countOfSingleHours = hours % hoursPerOneLamp;
        return getBerlinClockHourRepresentation(countOfSingleHours);
    }

    String getFiveHoursRow(String time) {
        final int hours = getHours(time);
        final int hoursPerOneLamp = 5;
        final int countOfFiveHours = hours / hoursPerOneLamp;
        return getBerlinClockHourRepresentation(countOfFiveHours);
    }

    String getSecondsLamp(String time) {
        int seconds = getSeconds(time);
        if(seconds % 2 == 0) {
            return "Y";
        } else {
            return "O";
        }
    }

    public int getMinutes(String time){
        return parsePosition(time, 3, 5);
    }

    public int getSeconds(String time){
        return parsePosition(time, 6, 8);
    }

    public int getHours(String time){
        return parsePosition(time, 0, 2);
    }

    private int parsePosition(String time, int from, int to){
        final String exceptionMessage = "Cannot parse substring between <" 
        + from + "," + to +") from given time=" + time;

        if(from < 0 || from > time.length() || from > to || to > time.length()){
            throw new IllegalArgumentException(exceptionMessage);
        }

        int parsedValue = 0;
        try{
            final String subsring = time.substring(from, to);
            parsedValue = Integer.parseInt(subsring);
        }catch (NumberFormatException ex){
            throw new IllegalArgumentException(exceptionMessage);
        }

        return parsedValue;
    }

    private String getBerlinClockHourRepresentation(int countOfHours){
        switch(countOfHours)
        {
            case 0:
                return "OOOO";
            case 1: 
                return "ROOO";
            case 2:
                return "RROO";
            case 3:
                return "RRRO";
            case 4:
                return "RRRR";
            default:
                return "OOOO"; // maybe throw exception
        }  
    }
}
